//-----------------------------------------------------------------------------
// Required Modules.
//-----------------------------------------------------------------------------
var zmq 	 = require("zmq");  
var _ 		 = require("underscore");
var mongoose = require('mongoose');
var models 	 = require('./MockSchema')(mongoose);
var SMF		 = require('./MockSMF');

//-----------------------------------------------------------------------------
// Configuration settings
// - todo : Move these to a standard config file, rather than needing code
//          changes to modify config.
//-----------------------------------------------------------------------------
var configDB = 'mongodb://localhost:27017/sensor2DB';
var configMQPort = 9997;
var configDBRetryWait = 5000;

//-----------------------------------------------------------------------------
// Global Variables (Never a good idea, but this is Javascript)
//-----------------------------------------------------------------------------
var gMQSocket = null;
var gPositions = null;
var gSystemHealthy = false;
var gRetryMessage = null;



//-----------------------------------------------------------------------------
// FUnction to write message to the database
//-----------------------------------------------------------------------------
function WriteMessageToDatabase(update,clientID)
{
	// The message from the mock ASG contains a client object containing all client
	// info. However, we have been passed the _ID of an entry in the positions database
	// collection to use, so remove the client from the JSON and add the ClientID.
	delete update.client;
	update.clientID = clientID;

	var request = new models.Request({  seqNum: update.seqNum,
										symbol: update.symbol,
										sourceServer: update.sourceServer,
										time: update.time,
										latency: update.latency,
										clientID: update.clientID});

	request.save(function(err){
		// Check to ensure we succeeded in writing to the database.
		if (err)
		{
			// Set callback to retry again.
			setTimeout(function(){
				WriteMessageToDatabase(update,clientID);
			},5000);

			if (!gSystemHealthy)
			{
				SMF.log('Failured to write as system is not healthy. Retry in 5 seconds');
			}
			else
			{
				SMF.log('Unexpected Error when attempting to write Request. Retry in 5 seconds');
				SMF.log(err);
				systemHealthy(false);
			}
		}
	});
}
 

//-----------------------------------------------------------------------------
// Function to process an ASG message.
//-----------------------------------------------------------------------------
function processASGMessage(message)
{
	// TODO This bext is just for debugging.
	// obviously logging every message is not something we'd want to do
	// in the real world.
	SMF.log("Received seq num: " + message.seqNum);

	if (gSystemHealthy)
	{
		var client = message.client;
		var clientID = 0
				
		var foundClient = _.findWhere(gPositions,{ip:client.ip,
										position:client.position});
		if (foundClient == undefined)
		{
			var pos = new models.Position({position:client.position,
											ip:client.ip,name:client.name});
			pos.save(function(err){
				if (err)
				{
					SMF.log('error on models.Position');
					throw err
				}
				else
				{
					clientID = pos._id;
					gPositions.push(pos);
					WriteMessageToDatabase(message,clientID);
					gMQSocket.send(message.seqNum);    
				}
			});
		}
		else
		{
			WriteMessageToDatabase(message,foundClient._id);
			gMQSocket.send(message.seqNum);    
		}
	}
	else
	{
		gRetryMessage = message; //Will be processed when system turns healthy
	}	
}

//-----------------------------------------------------------------------------
// Async event handler invoked on receipt of a message over the zeroMQ
//-----------------------------------------------------------------------------
function onMQReceiveMessage(data)
{
	data = data.toString('utf8');
	var message = JSON.parse(data);

	if (message.type.toLowerCase() == "asg")
	{
		processASGMessage(message.data)	
	}
	else
	{
		//TODO Taking the app down if someone sends us an unknown
		//message seems a bit extreme. Needs more thought.
		SMF.log(message);
		throw "Unknown Message Format Received";
	}

}

//-----------------------------------------------------------------------------
// Various simple Asyncronous Event Handlers. Title of functions say it all!
//-----------------------------------------------------------------------------
function onMQSocketBind(error) 
{  
    if (error) {
       SMF.log("Failed to bind socket: " + error.message);
       process.exit(0);
    } else {
        SMF.log("Server listening on port " + configMQPort);
    }
}

function onReceivePositionsData(err,data)
{
	if (err)
	{
		SMF.log('Error on attempt to get positions data');
		SMF.log(err);
		gPositions = [];
	}
	else
	{
		SMF.log('Positions data received successfully');
		gPositions = data;
	}

	systemHealthy(true);
}

function onDatabaseConnecting() 
{
	SMF.log('connecting to database');
}

function onDatabaseError(err)
{
  SMF.log('Database connection error: ' + err);
  systemHealthy(false);
}

function onDatabaseReconnected()
{
  SMF.log('Database connection re-established'); 
  systemHealthy(true);
}

function onDatabaseDisconnect()
{
  SMF.log('Database disconnected'); 
  systemHealthy(false);
}

//-----------------------------------------------------------------------------
// Called when a SIGINT signal is sent, typically someone terminating the process.
//-----------------------------------------------------------------------------
function onSIGINT()
{
	systemHealthy(false);
  	
  	mongoose.connection.close(function () { 
    	SMF.log('Database disconnected through app termination'); 
    	process.exit(0); 
	});
}

//-----------------------------------------------------------------------------
// Callback invoked when connection to database.
//-----------------------------------------------------------------------------
function onDatabaseConnect(err)
{
	if(err)
	{
		systemHealthy(false);
		SMF.log(err);
		SMF.log('Failed to connect to database - retrying in ' + configDBRetryWait + 'ms');
		
		setTimeout(DatabaseConnectWithRetry,configDBRetryWait);
	}
	else
	{
		SMF.log('Connected to Database');
		models.Position.find({}).exec(onReceivePositionsData);
		// Note: We set to healthy on completion of above callback.
	}
}

//-----------------------------------------------------------------------------
// Function to initialise the connection with the database.
//-----------------------------------------------------------------------------
function DatabaseConnectWithRetry()
{
  	mongoose.connect(configDB, {server:{socketOptions:{keepAlive: 1,
  							  connectTimeoutMS: configDBRetryWait }}}, onDatabaseConnect);
}

//-----------------------------------------------------------------------------
// Function to initialise the message queue
//-----------------------------------------------------------------------------
function MQInit()
{
	gMQSocket = zmq.socket("rep");

	gMQSocket.on("message", onMQReceiveMessage);
	gMQSocket.bind("tcp://*:" + configMQPort, onMQSocketBind);
}

//-----------------------------------------------------------------------------
// Function to handle state changes in system health. 
//-----------------------------------------------------------------------------
function systemHealthy(flag)
{
	gSystemHealthy = flag;
	SMF.log('System Healthy flag is now ' + flag);

	if (gSystemHealthy)
	{
		// If we have a message queued to process, process it.
		if (gRetryMessage)
		{
			processASGMessage(gRetryMessage);
			gRetryMessage = null;
		}
	}
}

//-----------------------------------------------------------------------------
// Start Here
//-----------------------------------------------------------------------------
DatabaseConnectWithRetry();
mongoose.connection.once('open',		onDatabaseConnect);
mongoose.connection.on  ('connecting', 	onDatabaseConnecting);
mongoose.connection.on  ('reconnected', onDatabaseReconnected);
mongoose.connection.on  ('error',		onDatabaseError); 
mongoose.connection.on  ('disconnected',onDatabaseDisconnect);

MQInit();

process.on('SIGINT', onSIGINT);
