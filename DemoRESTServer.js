//-----------------------------------------------------------------------------
// Requires
//-----------------------------------------------------------------------------
var _ 		 = require("underscore");
var mongoose = require('mongoose');
var express  = require('express');
var models 	 = require('./MockSchema')(mongoose);
var SMF		 = require('./MockSMF');

//-----------------------------------------------------------------------------
// Configuration settings
// - todo : Move these to a standard config file, rather than needing code
//          changes to modify config.
//-----------------------------------------------------------------------------
var configDB = 'mongodb://localhost:27017/sensor2DB';
var configDBRetryWait = 5000;

//-----------------------------------------------------------------------------
// Global Variables (Never a good idea, but this is Javascript)
//-----------------------------------------------------------------------------
var gSystemHealthy = false;
var gPositions = null;
var app = express();
var response = null;

//-----------------------------------------------------------------------------
// Simple GET request handler (for demo purposes only).
// Just returns the first 200 records from the request collection.
//-----------------------------------------------------------------------------
function onRouteGetAllSimple(req,res)
{	
	SMF.log('Processing Retrieve request');
	models.Request.find().limit(200).populate('clientID').exec(function(err,data){
		res.json(data);
	});
}

//-----------------------------------------------------------------------------
// GET request handler (for demo purposes only).
// Requires named parametes to be passed as part of the GET request.
// Specifically: 
// 		skip = number of documents to skip from start of results set.
//		limit = max number of documents to return.
//		startDate = unix time (milliseonds since 1970) to start from.
//		endDate = unix time (milliseonds since 1970) to end at.
//-----------------------------------------------------------------------------
function onRouteGetAll(req,res)
{	
	var skip = req.query.skip | 0;
	var limit = req.query.limit | 200;
	var startDate = req.query.startDate; 
	var endDate = req.query.endDate;

	SMF.log('Processing Retrieve request: range = ' + startDate + ' - ' + endDate);

	var query = {'time':{$gte:startDate,$lt:endDate}};
	var options = {skip: skip,limit:limit};


	models.Request.find(query).limit(limit).populate('clientID')
				  .exec(function(err,data){ 
				  	if (err)
				  	{
				  		SMF.log(err);
		  				res.status(500).send('Unable to service. Unexpected error');

				  	}
				  	else
				  	{
					  	res.json(data); 
				  	}
				 });
}

//-----------------------------------------------------------------------------
// Called at startup to get data from positions collection, rather than have
// to look up for every request received.
//-----------------------------------------------------------------------------
function onReceivePositionsData(err,data)
{
	gPositions = data;
	systemHealthy(true);
}

//-----------------------------------------------------------------------------
// This method is invoked when we enter a connecting state to the database.
// Nothing to do apart from logging this has happened.
//-----------------------------------------------------------------------------
function onDatabaseConnecting() 
{
	SMF.log('connecting to database');
}

//-----------------------------------------------------------------------------
// This method is invoked when we enter a connected state to the database.
// Kick off a call to query the positions collection (see onReceivePositionsData)
//-----------------------------------------------------------------------------
function onDatabaseConnect()
{
	SMF.log('Database connection established.');
	models.Position.find({}).exec(onReceivePositionsData);
}

//-----------------------------------------------------------------------------
// This method is invoked when we encounter an error interacting with the 
// database. Log this has happened and set application state to unhealthy.
//-----------------------------------------------------------------------------
function onDatabaseError(err)
{
  SMF.log('Database connection error: ' + err);
  systemHealthy(false);
}

//-----------------------------------------------------------------------------
// This method is invoked when we reestablish connection to the database.
// Log this has happened and set application state to healthy.
//-----------------------------------------------------------------------------
function onDatabaseReconnected()
{
  SMF.log('Database connection re-established'); 
  systemHealthy(true);
}

//-----------------------------------------------------------------------------
// This method is invoked when we disconnect from the database.
// Log this has happened and set application state to unhealthy.
//-----------------------------------------------------------------------------
function onDatabaseDisconnect()
{
  SMF.log('Database disconnected'); 
  systemHealthy(false);
}

//-----------------------------------------------------------------------------
// This method is invoked when a SIGINT signal is encounterd (probably a Ctrl-C)
// Log this has happened and terminate the application.
//-----------------------------------------------------------------------------
function onCtrlC()
{
  mongoose.connection.close(function () { 
    SMF.log('Database disconnected through app termination'); 
    process.exit(0); 
	});
}

function DatabaseConnectWithRetry()
{
  mongoose.connect(configDB, {server:{socketOptions:{keepAlive: 1,
  							  connectTimeoutMS: configDBRetryWait }}}, function(err){
		if(err)
		{
			SMF.log(err);
			SMF.log('Failed to connect to database - retrying in ' + configDBRetryWait + 'ms');
			
			setTimeout(DatabaseConnectWithRetry,configDBRetryWait);
		}
	});
}

// Single point in the code where we change and log system state. 
function systemHealthy(flag)
{
	gSystemHealthy = flag;
	SMF.log('System Healthy flag is now ' + flag);
}

function checkIfHealthy(req,res,next)
{
	if (!gSystemHealthy)
	{
		res.status(500).send('Unable to service. System is unhealthy');
		return;
	}
	next();
}

//-----------------------------------------------------------------------------
// Start Here
//-----------------------------------------------------------------------------
DatabaseConnectWithRetry();		//Establish connection to the database.

// Set up various asyncronous event handlers.
mongoose.connection.once('open',		onDatabaseConnect);
mongoose.connection.on  ('connecting', 	onDatabaseConnecting);
mongoose.connection.on  ('reconnected', onDatabaseReconnected);
mongoose.connection.on  ('error',		onDatabaseError); 
mongoose.connection.on  ('disconnected',onDatabaseDisconnect);
process.on('SIGINT', onCtrlC);

// Middleware intercept for for each request. Only allowing through if the system
// is healthy.
app.use(checkIfHealthy);

app.get('/',onRouteGetAllSimple);
app.get('/TIME',onRouteGetAll);

app.listen(3000,function()
{
	SMF.log('Listening on port 3000');
});


