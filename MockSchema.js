//-----------------------------------------------------------------------------
// This file defines the schemas for the collections to be held in the 
// mongo database.
//-----------------------------------------------------------------------------
module.exports = function(mongoose) {

	var positionSchema = new mongoose.Schema({
		name: String,
		ip: String,
		position: String
	});

	var Position = mongoose.model('Position',positionSchema);

	var requestSchema = mongoose.Schema({
		seqNum: Number,
		symbol:String,
		sourceServer: String,
		time: Date,
		latency:Number,
		clientID: {type:mongoose.Schema.Types.ObjectId,
			ref:'Position'}
	});

	var Request = mongoose.model('Request',requestSchema);

    var models = {
      Position : mongoose.model('Position', Position),
      Request : mongoose.model('Request', Request)
    };

    return models;
}