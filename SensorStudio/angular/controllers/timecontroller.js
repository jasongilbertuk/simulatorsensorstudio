(function(){
	var dbService = null;
	var scope = null;

	function refreshData()
	{
		var data = dbService.getAllEntries().success(function(data){
			scope.allEntries = data;
		});
	}

	function initDatePicker()
	{
		// Apply the date range picker with default settings to the button
		$('#picker').daterangepicker();

		// Apply the date range picker with custom settings to the button
		$('#picker').daterangepicker({
		    format: 'MM/DD/YYYY',
		    startDate: moment().subtract(29, 'days'),
		    endDate: moment(),
		    minDate: '01/12/2015',
		    maxDate: '12/31/2100',
		    dateLimit: { days: 60 },
		    showDropdowns: true,
		    showWeekNumbers: true,
		    timePicker: true,
		    timePickerIncrement: 1,
		    timePicker12Hour: true,
		    ranges: {
		       'Last hour': [moment().subtract(1, 'hours'), moment()],
		       'Last 24 hours': [moment().subtract(1, 'days'), moment()],
		       'Last 7 Days': [moment().subtract(6, 'days'), moment()],
		       'Last 30 Days': [moment().subtract(29, 'days'), moment()],
		       'This Month': [moment().startOf('month'), moment().endOf('month')],
		       'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
		    },
		    opens: 'left',
		    drops: 'down',
		    buttonClasses: ['btn', 'btn-sm'],
		    applyClass: 'btn-primary',
		    cancelClass: 'btn-default',
		    separator: ' to ',
		    locale: {
		        applyLabel: 'Submit',
		        cancelLabel: 'Cancel',
		        fromLabel: 'From',
		        toLabel: 'To',
		        customRangeLabel: 'Custom',
		        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
		        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		        firstDay: 1
		    }
		}, function(start, end, label) {

			var startString = start.toISOString();
			var endString = end.toISOString();
			$('#selectedRange').val(start.format('MMMM D, YYYY H:mm') + ' - ' + end.format('MMMM D, YYYY H:mm'));
			initTable(startString,endString);
		});
	}

	function initTable(startDate,endDate)
	{
		dbService.getAllEntries(0,200,startDate,endDate).success(function(data){
			scope.allEntries = data;
		}).error(function(err){
			//todo handle error
		});
	}

	function setSortColumn(columnName)
	{
		scope.sortColumn = columnName;
	}

	//-------------------------------------------------------------------------
	// constructor - initalisation of the controller.
	//-------------------------------------------------------------------------
	function timecontroller($scope,databaseService)
	{
		scope = $scope;
		scope.allEntries = {};
		dbService = databaseService;
		setSortColumn("time");	// By default, sort order is the time column
		scope.setSortColumn = setSortColumn;

		initDatePicker();
	}

	angular.module('sensorstudioapp').controller('timecontroller',timecontroller);
})();
