
(function(){
	function databaseService($http)
	{
 		this.getAllEntries = function(skip,limit,startDate,endDate)
 		{
 			//TODO: Need to get the Base URL from configuration.
 			var requestString = 'http://localhost:3000/TIME?'+
	 			'skip='+skip+
	 			'&limit='+limit+
	 			'&startDate='+startDate+
	 			'&endDate='+endDate;

	 		return $http.get(requestString);
	    }
	}

	angular.module('sensorstudioapp').service('databaseService',databaseService);

})();