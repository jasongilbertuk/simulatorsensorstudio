(function(){
	var app = angular.module('sensorstudioapp',['ngRoute','ui.bootstrap','xeditable','angularGrid','angularModalService']);

	app.run(function(editableOptions) {
    editableOptions.theme = 'bs3'; 
});

})();