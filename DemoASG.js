//-----------------------------------------------------------------------------
// Requires
//-----------------------------------------------------------------------------
var zmq = require('zmq');
var socket = zmq.socket('req');

//-----------------------------------------------------------------------------
// Global Variables
//-----------------------------------------------------------------------------
var count = 0;
var recvCount = 0;
var prevRecvCount = 0;
var port = 9997;
var clients = [ {name:"Marty Stan Lee",ip: "255.1.27.35",position:"1"},
				{name:"Marty Stan Lee",ip: "255.1.27.35",position:"2"},
				{name:"Marty Stan Lee",ip: "255.1.27.35",position:"3"},
				{name:"Marty Stan Lee",ip: "255.1.27.35",position:"4"},
				{name:"Marty Stan Lee",ip: "255.1.27.35",position:"5"},
				{name:"RBW",ip: "219.17.41.8",position:"1"},
				{name:"RBW",ip: "219.17.41.8",position:"2"},
				{name:"RBW",ip: "219.17.41.8",position:"3"},
				{name:"RBW",ip: "219.17.41.8",position:"4"},
				{name:"RBW",ip: "219.17.41.8",position:"5"},
				{name:"Chuck Stream",ip: "255.62.47.41",position:"1"},
				{name:"Chuck Stream",ip: "255.62.47.41",position:"2"},
				{name:"Chuck Stream",ip: "255.62.47.41",position:"3"},
				{name:"Chuck Stream",ip: "255.62.47.41",position:"4"},
				{name:"Chuck Stream",ip: "255.62.47.41",position:"5"},
				{name:"Chuck Stream",ip: "255.62.47.42",position:"1"},
				{name:"Chuck Stream",ip: "255.62.47.42",position:"2"},
				{name:"Chuck Stream",ip: "255.62.47.42",position:"3"},
				{name:"Chuck Stream",ip: "255.62.47.42",position:"4"},
				{name:"Chuck Stream",ip: "255.62.47.42",position:"5"},
				{name:"FLYNN.COM",ip: "255.192.121.2",position:"1"},
				{name:"FLYNN.COM",ip: "255.192.121.2",position:"2"},
				{name:"FLYNN.COM",ip: "255.192.121.2",position:"3"},
				{name:"FLYNN.COM",ip: "255.192.121.2",position:"4"},
				{name:"FLYNN.COM",ip: "255.192.121.2",position:"5"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"1"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"2"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"3"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"4"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"5"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"6"},
				{name:"Barrys Capital",ip: "255.13.32.9",position:"7"},
				{name:"Barrys  Capital",ip: "255.13.32.9",position:"8"}
];

var RICs = ["IBM.L","MSFT.O","0#FTSE","TRI.N"];

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

//-----------------------------------------------------------------------------
// Simulate a message from the ASG
//-----------------------------------------------------------------------------
function generateMessage()
{
	var data = {};
	data.seqNum = count++;
	var clientIndex = getRandomInt(0,clients.length);
	var ricIndex = getRandomInt(0,RICs.length);
	data.client= clients[clientIndex];
	data.symbol = RICs[ricIndex];
	data.sourceServer = "ASG:" + port;
	data.time = Date();
	data.latency = getRandomInt(50,250);

	var message = {};
	message.type="asg";
	message.data = data;

	return JSON.stringify(message);
}

//-----------------------------------------------------------------------------
// Async callback every timer configured interval.
//-----------------------------------------------------------------------------
function onTick()
{
	console.log("Sent: "+ count + " Acks: " + recvCount);
	var message = generateMessage();
	socket.send(message);
}

//-----------------------------------------------------------------------------
// Async callback every timer configured interval.
//-----------------------------------------------------------------------------
function onCheckHealth()
{
	if (prevRecvCount == recvCount)
	{
		console.log("ALERT: No Acks received. Check connectivity to Sensor Server");
	}
	else
	{
		prevRecvCount = recvCount;
	}
}

socket.on('message',function(message){
	recvCount++;
});

//TODO: Address should be a configuration value
socket.connect('tcp://127.0.0.1:9997');  
setInterval(onTick,process.argv[2]);
setInterval(onCheckHealth,2000);


